#include "FissileIsotopeParser.h"

FissileIsotopeParser::FissileIsotopeParser(Lexer &lex){
  lexer = &lex;
}

bool FissileIsotopeParser::parse(){

  try {
    if (lexer->getNextToken() != token::FISSILE_ISOTOPE){
      printf("Expected FissileIsotope Keyword.\n");
      return false;
    }
    root = new FissileIsotopeNode(lexer->getCurrentTokenValue());
    
    AbstractParser* childParser = new IntegerParser(*lexer);
    if(!childParser->parse()){
      printf("Expected a number after Fissile Isotope.\n");
      return false;
    }
    root->addChild(childParser->getRoot());
  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* FissileIsotopeParser::getRoot() const {
  return root;
}
