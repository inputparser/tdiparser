#include "VelocityNode.h"

VelocityNode::VelocityNode (std::string v) : value(v) {
  xsNode = new XSNode(value);
}

std::string VelocityNode::getValue() const {
  return value;
}

unsigned int VelocityNode::getNumberOfChildren() const {
  return xsNode->getNumberOfChildren();
}

AbstractParseNode* VelocityNode::getChild(unsigned int index) const {
  if (index >= xsNode->getNumberOfChildren())
    throw ChildOverReachException("VelocityNode", index, xsNode->getNumberOfChildren());

  return xsNode->getChild(index);
}

void VelocityNode::addChild(AbstractParseNode* child){
  try {
    xsNode->addChild(child);
  } catch (Exception &e) {
    e.printErrorReport();
    throw e;
  }
}

void VelocityNode::printValue (std::ostream &out) const {
  xsNode->printValue(out);
}
