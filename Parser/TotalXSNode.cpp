#include "TotalXSNode.h"

TotalXSNode::TotalXSNode (std::string v) : value(v) {
  xsNode = new XSNode(value);
}

std::string TotalXSNode::getValue() const {
  return value;
}

unsigned int TotalXSNode::getNumberOfChildren() const {
  return xsNode->getNumberOfChildren();
}

AbstractParseNode* TotalXSNode::getChild(unsigned int index) const {
  if (index >= xsNode->getNumberOfChildren())
    throw ChildOverReachException("TotalXSNode", index, xsNode->getNumberOfChildren());

  return xsNode->getChild(index);
}

void TotalXSNode::addChild(AbstractParseNode* child){
  try{
    xsNode->addChild(child);
  } catch (Exception &e) {
    e.printErrorReport();
    throw e;
  }
}

void TotalXSNode::printValue (std::ostream& out) const {
  xsNode->printValue(out);
}

