#ifndef FISSION_XS_NODE_H_HJ76
#define FISSION_XS_NODE_H_HJ76

#include "AbstractParseNode.h"
#include "XSNode.h"

class FissionXSNode : public AbstractParseNode {

  public:
    FissionXSNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue (std::ostream&) const;

  private:
    const std::string value;
  //  unsigned int numberOfChildren;
  //  std::vector<AbstractParseNode*> children;
    AbstractParseNode* xsNode;
};



#endif // FISSION_XS_NODE_H_HJ76
