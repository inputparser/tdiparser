#include "FissionSpectrumNode.h"

FissionSpectrumNode::FissionSpectrumNode (std::string v) : value(v) {
  xsNode = new XSNode(value);
}

std::string FissionSpectrumNode::getValue() const {
  return value;
}

unsigned int FissionSpectrumNode::getNumberOfChildren() const {
  return xsNode->getNumberOfChildren();
}

AbstractParseNode* FissionSpectrumNode::getChild(unsigned int index) const {
  if (index >= xsNode->getNumberOfChildren())
    throw ChildOverReachException ("FissionSpectrumNode",index,xsNode->getNumberOfChildren());

  return xsNode->getChild(index);
}

void FissionSpectrumNode::addChild(AbstractParseNode* child){
  try {
    xsNode->addChild(child);
  } catch (Exception &e) {
    e.printErrorReport();
    throw e;
  }
}

void FissionSpectrumNode::printValue (std::ostream &out) const {
  xsNode->printValue(out);
}
