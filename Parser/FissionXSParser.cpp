#include "FissionXSParser.h"

FissionXSParser::FissionXSParser(Lexer &lex){
  lexer = &lex;
}

bool FissionXSParser::parse(){

  try {
    if (lexer->getNextToken() != token::FISSION_XS){
      printf("Expected NuFission Keyword.\n");
      return false;
    }
    root = new FissionXSNode(lexer->getCurrentTokenValue());

    while(lexer->peek() == token::NUMBER){
      AbstractParser* childParser = new ValueParser(*lexer);
      if(!childParser->parse()){
        printf("Value Parser Failed in Fission XS.\n");
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* FissionXSParser::getRoot() const {
  return root;
}
