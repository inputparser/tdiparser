#include "KineticSolverNode.h"

KineticSolverNode::KineticSolverNode (std::string v) : value(v), numberOfChildren(0) {}

std::string KineticSolverNode::getValue() const {
  return value;
}

unsigned int KineticSolverNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* KineticSolverNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("KineticSolverNode", index, numberOfChildren);

  return children[index];
}

void KineticSolverNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}


