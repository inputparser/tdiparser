#include "../MacroscopicCrossSectionParser.h"
#include <sstream>

void writeInput (std::iostream &input){
	input << "#Comment" << std::endl;
	input << "" << std::endl;
	input << "MacroscopicCrossSections[     8     2"                                    << std::endl;
	input << ""                                                                         << std::endl;
	input << "Velocity"                                                                 << std::endl;
	input << "  3.071837E+09  4.601048E+08  2.768231E+07  1.802294E+06  9.006059E+05"   << std::endl;
	input << "  6.185222E+05  4.185608E+05  1.016622E+05"                               << std::endl;
	input << ""                                                                         << std::endl;
	input << "Fuel"                                                                     << std::endl;
	input << "FissileIsotopes     2"                                                    << std::endl;
	input << "Total"                                                                    << std::endl;
	input << "  1.176353E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01"   << std::endl;
	input << "  7.354003E-01  8.853813E-01  1.310367E+00"                               << std::endl;
	input << "FissionSpectrum"                                                          << std::endl;
	input << "  7.615390E-01  2.382807E-01  1.806897E-04  1.310741E-08  1.339869E-09"   << std::endl;
	input << "  5.437149E-10  3.184616E-10  2.252533E-10  7.666079E-01  2.332288E-01"   << std::endl;
	input << "  1.637666E-04  2.988133E-09  1.368858E-10  3.763368E-11  1.444842E-11"   << std::endl;
	input << "  2.802130E-12"                                                           << std::endl;
	input << "NuFission"                                                                << std::endl;
	input << "  2.628513E-04  3.221616E-04  4.318228E-03  7.441536E-03  2.694571E-02"   << std::endl;
	input << "  4.088106E-02  5.614505E-02  1.077831E-01  2.554787E-03  3.017494E-06"   << std::endl;
	input << "  1.105671E-06  1.128672E-08  1.847252E-08  2.663696E-08  3.804506E-08"   << std::endl;
	input << "  6.680804E-08"                                                           << std::endl;
	input << "Transfer    1"                                                            << std::endl;
	input << "Profile"                                                                  << std::endl;
	input << "     1     1     1     2     1     4     1     8     1     8     1     8" << std::endl;
	input << "     1     8     2     8"                                                 << std::endl;
	input << ""                                                                         << std::endl;
	input << "  7.458998E-02  4.147714E-02  2.782045E-01  2.271082E-04  4.734908E-02"   << std::endl;
	input << "  4.720232E-01  1.222379E-04  9.926890E-08  2.848432E-05  4.325281E-02"   << std::endl;
	input << "  3.515164E-01  5.942782E-03  1.484443E-05  2.809442E-06  1.513591E-06"   << std::endl;
	input << "  1.096059E-14  2.918276E-06  4.333786E-03  1.123040E-01  3.212802E-01"   << std::endl;
	input << "  2.981855E-02  4.150550E-03  2.023440E-03  9.237545E-16  1.180266E-06"   << std::endl;
	input << "  1.758638E-03  4.073118E-02  1.717842E-01  3.844078E-01  9.963744E-02"   << std::endl;
	input << "  4.167538E-02  1.608478E-16  6.861544E-07  1.030060E-03  2.020496E-02"   << std::endl;
	input << "  7.357193E-02  2.143690E-01  5.364580E-01  2.641490E-01  3.533834E-07"   << std::endl;
	input << "  7.285041E-04  8.423053E-03  2.877435E-02  7.250318E-02  1.976911E-01"   << std::endl;
	input << "  9.138074E-01"                                                           << std::endl;
	input << ""                                                                         << std::endl;
	input << "  3.813157E-02  1.557110E-02  1.460048E-01  1.268888E-05  2.054424E-02"   << std::endl;
	input << "  2.673745E-01  5.792475E-05  3.285600E-08  1.540217E-06  2.019882E-02"   << std::endl;
	input << "  2.086192E-01  3.012258E-03  3.343568E-06  2.007192E-07 -1.063276E-08"   << std::endl;
	input << "  0.000000E+00  2.769354E-07  9.517769E-04  4.757602E-02  1.811931E-01"   << std::endl;
	input << "  1.133483E-02  4.326158E-04 -7.492836E-05  0.000000E+00  1.551413E-07"   << std::endl;
	input << "  2.670707E-04  7.997961E-03  5.811786E-02  1.898135E-01  2.115588E-02"   << std::endl;
	input << " -6.668323E-04  0.000000E+00  1.247888E-07  1.101829E-04  1.424596E-03"   << std::endl;
	input << "  6.384740E-03  4.355518E-02  2.052779E-01  1.331473E-02  1.020462E-07"   << std::endl;
	input << "  4.806624E-05  7.637318E-05 -1.116338E-03 -1.128642E-03  9.683650E-03"   << std::endl;
	input << "  1.985224E-01"                                                           << std::endl;
	input << ""                                                                         << std::endl;
	input << "Water"                                                                    << std::endl;
	input << "FissileIsotopes     0"                                                    << std::endl;
	input << "Total"                                                                    << std::endl;
	input << "  1.964504E-01  6.273928E-01  1.058545E+00  1.112821E+00  1.272213E+00"   << std::endl;
	input << "  1.504232E+00  1.823061E+00  2.713561E+00"                               << std::endl;
	input << "Transfer    1"                                                            << std::endl;
	input << "Profile"                                                                  << std::endl;
	input << "     1     1     1     2     1     4     1     8     2     8     2     8" << std::endl;
	input << "     2     8     2     8"                                                 << std::endl;
	input << ""                                                                         << std::endl;
	input << "  1.141525E-01  8.143625E-02  5.214642E-01  5.078717E-04  1.058142E-01"   << std::endl;
	input << "  9.429234E-01  1.350074E-04  2.231832E-07  6.404112E-05  9.658839E-02"   << std::endl;
	input << "  6.984070E-01  1.089213E-02  3.275240E-05  6.313287E-06  3.402996E-06"   << std::endl;
	input << "  6.561140E-06  9.743622E-03  2.488409E-01  6.367791E-01  6.183022E-02"   << std::endl;
	input << "  9.200823E-03  4.545829E-03  2.653584E-06  3.953934E-03  9.156797E-02"   << std::endl;
	input << "  3.765631E-01  7.844948E-01  2.138419E-01  9.297968E-02  1.542677E-06"   << std::endl;
	input << "  2.315876E-03  4.542661E-02  1.651665E-01  4.687702E-01  1.126945E+00"   << std::endl;
	input << "  5.759703E-01  7.945096E-07  1.637891E-03  1.893748E-02  6.469066E-02"   << std::endl;
	input << "  1.626071E-01  4.347659E-01  1.970808E+00"                               << std::endl;
	input << ""                                                                         << std::endl;
	input << "  6.510711E-02  3.638660E-02  3.155068E-01  2.853294E-05  4.638400E-02"   << std::endl;
	input << "  5.988253E-01  1.193550E-04  7.386981E-08  3.462858E-06  4.561189E-02"   << std::endl;
	input << "  4.661889E-01  6.990710E-03  7.961499E-06  4.538153E-07 -2.390555E-08"   << std::endl;
	input << "  6.226319E-07  2.139874E-03  1.078032E-01  4.032091E-01  2.631161E-02"   << std::endl;
	input << "  1.046497E-03 -1.660377E-04  3.488031E-07  6.004533E-04  1.798732E-02"   << std::endl;
	input << "  1.324493E-01  4.218481E-01  4.917897E-02 -1.140834E-03  2.805619E-07"   << std::endl;
	input << "  2.477235E-04  3.202952E-03  1.449487E-02  9.978430E-02  4.565563E-01"   << std::endl;
	input << "  3.253001E-02  2.294298E-07  1.080670E-04  1.717164E-04 -2.508058E-03"   << std::endl;
	input << " -2.338708E-03  2.302927E-02  4.415402E-01"                               << std::endl;
	input << ""                                                                         << std::endl;
	input << "]"                                                                        << std::endl;
        input << ""                                                                         << std::endl;
        input << ""                                                                         << std::endl;
	input << ""                                                                         << std::endl;
}


int main (){

	std::stringstream input;
	writeInput(input);

	Lexer lexer(input);
	if(!lexer.lex()){
		printf("Lexer failed.\n");
		return 1;
	}

	AbstractParser* parser = new MacroscopicCrossSectionParser(lexer);
	if(!parser->parse()){
		printf("Parser failed.\n");
		return 1;
	}

	AbstractParseNode* root = parser->getRoot();
	if(root->getValue() != "MacroscopicCrossSections"){
		printf("Value not as expected.\n"
			   "\tExpected: %s\n"
			   "\tResult:   %s\n", "MacroscopicCrossSections", root->getValue().c_str());
		return 1;
	}
	if(root->getNumberOfChildren() != 5){
		printf("Number of Children not as expected.\n"
			   "\tExpected: %d\n"
			   "\tResult:   %d\n", 5, root->getNumberOfChildren());
		return 1;
	}
	if(root->getChild(0)->getValue() != "8"){
                printf("Node Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "8", root->getChild(0)->getValue().c_str());
                return 1;
	}
        if(root->getChild(1)->getValue() != "2"){
                printf("Node Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "2", root->getChild(1)->getValue().c_str());
                return 1;
        }
	if(root->getChild(2)->getValue() != "Velocity"){
                printf("Node Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "Velocity", root->getChild(2)->getValue().c_str());
                return 1;
	}
        if(root->getChild(3)->getValue() != "Fuel"){
                printf("Node Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "Fuel", root->getChild(3)->getValue().c_str());
                return 1;
        }
        if(root->getChild(4)->getValue() != "Water"){
                printf("Node Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "Water", root->getChild(4)->getValue().c_str());
                return 1;
        }
	
	// Print TotalXS for Fuel and Water
	std::cout << "\t";
	root->getChild(2)->printValue(std::cout);
	std::cout << "Fuel XS\n";
	std::cout << "\t";
	root->getChild(3)->getChild(1)->printValue(std::cout);
	std::cout << "\t";
        root->getChild(3)->getChild(2)->printValue(std::cout);
        std::cout << "\t";
        root->getChild(3)->getChild(3)->printValue(std::cout);
	std::cout << "\t";
        root->getChild(3)->getChild(4)->printValue(std::cout);
        std::cout << "Water XS\n";
        std::cout << "\t";
        root->getChild(4)->getChild(1)->printValue(std::cout);
        std::cout << "\t";
        root->getChild(4)->getChild(2)->printValue(std::cout);




	return 0;
}
