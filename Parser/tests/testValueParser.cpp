#include "../ValueParser.h"
#include <sstream>

void writeInput (std::iostream &input){
	input << "#Comment" << std::endl;
	input << "" << std::endl;
	input << "KineticSolver[ 0.56 ]" << std::endl;
	input << "" << std::endl;
	input << "" << std::endl;
}


int main (){

	std::stringstream input;
	writeInput(input);

	Lexer lexer(input);
	if(!lexer.lex()){
		printf("Lexer failed.\n");
		return 1;
	}

	// Skip over the first two tokens.
	int initialTokens = lexer.getNextToken();

	AbstractParser* parser = new ValueParser(lexer);
	if(!parser->parse()){
		printf("Parser failed.\n");
		return 1;
	}

	AbstractParseNode* root = parser->getRoot();
	if(root->getValue() != "0.56"){
		printf("Value not as expected.\n"
			   "\tExpected: %s\n"
			   "\tResult:   %s\n", "0.56", root->getValue().c_str());
		return 1;
	}
	if(root->getNumberOfChildren() != 0){
		printf("Value not as expected.\n"
			   "\tExpected: %d\n"
			   "\tResult:   %d\n", 0, root->getNumberOfChildren());
		return 1;
	}

	delete parser;

	return 0;
}
