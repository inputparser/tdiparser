#include "../KineticSolverParser.h"
#include <sstream>

void writeInput (std::iostream &input){
	input << "#Comment" << std::endl;
	input << "" << std::endl;
	input << "KineticSolver[ 0.56 ]" << std::endl;
	input << "" << std::endl;
	input << "" << std::endl;
}


int main (){

	std::stringstream input;
	writeInput(input);

	Lexer lexer(input);
	if(!lexer.lex()){
		printf("Lexer failed.\n");
		return 1;
	}

	AbstractParser* parser = new KineticSolverParser(lexer);
	if(!parser->parse()){
		printf("Parser failed.\n");
		return 1;
	}

	AbstractParseNode* root = parser->getRoot();
	if(root->getValue() != "KineticSolver"){
		printf("Value not as expected.\n"
			   "\tExpected: %s\n"
			   "\tResult:   %s\n", "KineticSolver", root->getValue().c_str());
		return 1;
	}
	if(root->getNumberOfChildren() != 1){
		printf("Value not as expected.\n"
			   "\tExpected: %d\n"
			   "\tResult:   %d\n", 1, root->getNumberOfChildren());
		return 1;
	}
        if(root->getChild(0)->getValue() != "0.56"){
                printf("Value not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "0.56", root->getChild(0)->getValue().c_str());
                return 1;
        }


	return 0;
}
