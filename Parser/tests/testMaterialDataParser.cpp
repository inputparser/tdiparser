#include "../MaterialDataParser.h"
#include <sstream>

void writeInput (std::iostream &input){
	input << "#Comment" << std::endl;
	input << "" << std::endl;
        input << "MacroscopicCrossSections[ 2 8" << std::endl;
	input << "Fuel" << std::endl;
        input << "FissileIsotopes   2" << std::endl;
        input << "Total" << std::endl;
        input << "  1.176353E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01" << std::endl;
        input << "  7.354003E-01  8.853813E-01  1.310367E+00" << std::endl;
        input << "NuFission" << std::endl;
        input << "  3.263684E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01" << std::endl;
        input << "  7.354003E-01  8.853813E-01  1.310367E+00  7.354003E-01  8.853813E-01" << std::endl;
        input << "  1.176353E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01" << std::endl;
        input << "  1.310367E+00" << std::endl;
        input << "FissionSpectrum" << std::endl;
        input << "  8.853813E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01" << std::endl;
        input << "  7.354003E-01  8.853813E-01  1.310367E+00  7.354003E-01  8.853813E-01" << std::endl;
        input << "  1.176353E-01  3.263684E-01  5.329260E-01  5.426784E-01  6.241585E-01" << std::endl;
        input << "  1.310367E+00" << std::endl;
        input << "" << std::endl;
        input << "" << std::endl;
	input << "]" << std::endl;
	input << "" << std::endl;
}


int main (){

	std::stringstream input;
	writeInput(input);

	Lexer lexer(input);
	if(!lexer.lex()){
		printf("Lexer failed.\n");
		return 1;
	}

	lexer.advanceTokenStream();
	lexer.advanceTokenStream();
	lexer.advanceTokenStream();

	AbstractParser* parser = new MaterialDataParser(lexer);
	if(!parser->parse()){
		printf("Parser failed.\n");
		return 1;
	}

	AbstractParseNode* root = parser->getRoot();
	if(root->getValue() != "Fuel"){
		printf("Value not as expected.\n"
			   "\tExpected: %s\n"
			   "\tResult:   %s\n", "Fuel", root->getValue().c_str());
		return 1;
	}
	if(root->getNumberOfChildren() != 4){
		printf("Number of Children not as expected.\n"
			   "\tExpected: %d\n"
			   "\tResult:   %d\n", 4, root->getNumberOfChildren());
		return 1;
	}
        if(root->getChild(0)->getChild(0)->getValue() != "2"){
                printf("Number of Fissiles not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "2", root->getChild(0)->getChild(0)->getValue().c_str());
                return 1;
        }
        // Total XS
	if(root->getChild(1)->getChild(0)->getValue() != "1.176353E-01"){
               printf("First Total XS not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "1.176353E-01", root->getChild(1)->getChild(0)->getValue().c_str());
                return 1;
	}
	// Print TotalXS to screen
	std::cout << "\t";
	root->getChild(1)->printValue(std::cout);
	// Fission XS
        if(root->getChild(2)->getChild(0)->getValue() != "3.263684E-01"){
               printf("First Fission XS not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "3.263684E-01", root->getChild(2)->getChild(0)->getValue().c_str());
                return 1;
        }
        // Print FissionXS to screen
        std::cout << "\t";
        root->getChild(2)->printValue(std::cout);
        // Fission Spectrum
        if(root->getChild(3)->getChild(0)->getValue() != "8.853813E-01"){
               printf("First Fission XS not as expected.\n"
                           "\tExpected: %s\n"
                           "\tResult:   %s\n", "8.853813E-01", root->getChild(3)->getChild(0)->getValue().c_str());
                return 1;
        }
        // Print FissionSpectrum to screen
        std::cout << "\t";
        root->getChild(3)->printValue(std::cout);


	return 0;
}
