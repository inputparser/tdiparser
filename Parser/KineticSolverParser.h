#ifndef KINETIC_SOLVER_PARSER_H_3R21
#define KINETIC_SOLVER_PARSER_H_3R21

#include "AbstractParser.h"
#include "KineticSolverNode.h"
#include "ValueParser.h"
#include "../Lexer/Lexer.h"

class KineticSolverParser : public AbstractParser {

  public:
    KineticSolverParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // KINETIC_SOLVER_PARSER_H_3R21
