#include "ValueParser.h"

ValueParser::ValueParser(Lexer &lex){
  lexer = &lex;
}

bool ValueParser::parse(){
  if (lexer->getNextToken() != token::NUMBER){
    printf("Expected a Number.\n");
    return false;
  }

  root = new ValueNode(lexer->getCurrentTokenValue());

  return true;
}

AbstractParseNode* ValueParser::getRoot() const {
  return root;
}
