#ifndef ABSTRACT_PARSE_NODE_H_7AS5
#define ABSTRACT_PARSE_NODE_H_7AS5

#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <ostream>
#include "../Exceptions/ChildOverReachException.h"

class AbstractParseNode {

  public:
    virtual std::string getValue () const = 0;
    virtual unsigned int getNumberOfChildren () const = 0;
    virtual AbstractParseNode* getChild (unsigned int) const = 0;
    virtual void addChild (AbstractParseNode*) = 0;

    virtual void printValue(std::ostream &out) const{
      out << "Value: " << getValue() << std::endl;
    }

};

#endif // ABSTRACT_PARSE_NODE_H_7AS5
