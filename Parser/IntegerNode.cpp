#include "IntegerNode.h"

IntegerNode::IntegerNode (std::string v) : value(v), numberOfChildren(0) {}

std::string IntegerNode::getValue() const {
  return value;
}

unsigned int IntegerNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* IntegerNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException ("IntegerNode", index, numberOfChildren);

  return children[index];
}

void IntegerNode::addChild(AbstractParseNode* child){
//  children.push_back(child);
//  numberOfChildren++;
}


