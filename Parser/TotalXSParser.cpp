#include "TotalXSParser.h"

TotalXSParser::TotalXSParser(Lexer &lex){
  lexer = &lex;
}

bool TotalXSParser::parse(){

  try {
    if (lexer->getNextToken() != token::TOTAL_XS){
      printf("Expected Total Keyword.\n");
      return false;
    }
    root = new TotalXSNode(lexer->getCurrentTokenValue());

    while(lexer->peek() == token::NUMBER){
      AbstractParser* childParser = new ValueParser(*lexer);
      if(!childParser->parse()){
        printf("Value Parser Failed in Total XS.\n");
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* TotalXSParser::getRoot() const {
  return root;
}
