#ifndef FISSILE_ISOTOPE_PARSER_H_987J
#define FISSILE_ISOTOPE_PARSER_H_987J

#include "AbstractParser.h"
#include "FissileIsotopeNode.h"
#include "IntegerParser.h"
#include "../Lexer/Lexer.h"

class FissileIsotopeParser : public AbstractParser {

  public:
    FissileIsotopeParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // FISSILE_ISOTOPE_PARSER_H_987J

