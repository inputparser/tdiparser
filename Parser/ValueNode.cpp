#include "ValueNode.h"

ValueNode::ValueNode (std::string v) : value(v), numberOfChildren(0) {}

std::string ValueNode::getValue() const {
  return value;
}

unsigned int ValueNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* ValueNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException ("ValueNode", index, numberOfChildren);

  return children[index];
}

void ValueNode::addChild(AbstractParseNode* child){
//  children.push_back(child);
//  numberOfChildren++;
}


