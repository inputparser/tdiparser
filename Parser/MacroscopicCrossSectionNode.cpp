#include "MacroscopicCrossSectionNode.h"

MacroscopicCrossSectionNode::MacroscopicCrossSectionNode (std::string v) : value(v), numberOfChildren(0) {}

std::string MacroscopicCrossSectionNode::getValue() const {
  return value;
}

unsigned int MacroscopicCrossSectionNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* MacroscopicCrossSectionNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("MacroscopicCrossSectionNode", index, numberOfChildren);

  return children[index];
}

void MacroscopicCrossSectionNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}


