#ifndef ABSTRACT_PARSER_H_9DH6
#define ABSTRACT_PARSER_H_9DH6

#include "AbstractParseNode.h"

class AbstractParser {

  public:
    virtual bool parse () = 0;
    virtual AbstractParseNode* getRoot () const = 0;
};

#endif // ABSTRACT_PARSER_H_9DH6
