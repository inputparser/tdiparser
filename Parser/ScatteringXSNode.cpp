#include "ScatteringXSNode.h"

ScatteringXSNode::ScatteringXSNode (std::string v) : value(v), numberOfChildren(0) {}

std::string ScatteringXSNode::getValue() const {
  return value;
}

unsigned int ScatteringXSNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* ScatteringXSNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("ScatteringXSNode", index, numberOfChildren);

  return children[index];
}

void ScatteringXSNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}

void ScatteringXSNode::printValue (std::ostream& out) const {

  unsigned int nAnisotropy = atoi(children[0]->getValue().c_str());
  unsigned int nGroups = children[1]->getNumberOfChildren()/2;
  unsigned int counter = 0;
  out << "Scattering Cross Section:" << std::endl;
  
  for (unsigned int k = 0; k <= nAnisotropy; k++){
    out << "Anisotropy Order: " << k << std::endl;
    for (unsigned int i = 0; i < nGroups; i++){
      unsigned int begin = atoi(children[1]->getChild(2*i)->getValue().c_str());
      unsigned int end = atoi(children[1]->getChild(2*i+1)->getValue().c_str());

      out << "  ";
      for (unsigned int j = 1; j < begin; j++)
  out << "0.0" << std::string(children[2]->getValue().length()-1, ' ');
      for (unsigned int j = begin; j <= end; j++){
  out << children[2+counter]->getValue() << "  ";
  counter++;
      }
      for (unsigned int j = end; j < nGroups; j++)
  out << "0.0" << std::string(children[2]->getValue().length()-1, ' ');
      out << std::endl;
    }
  }

}

