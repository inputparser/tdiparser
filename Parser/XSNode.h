#ifndef XS_NODE_H_3VFF
#define XS_NODE_H_3VFF

#include "AbstractParseNode.h"
#include <vector>

class XSNode : public AbstractParseNode {

  public:
    XSNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue (std::ostream&) const;

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // XS_NODE_H_3VFF
