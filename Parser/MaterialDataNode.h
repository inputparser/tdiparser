#ifndef MATERIAL_DATA_NODE_H_9E4G
#define MATERIAL_DATA_NODE_H_9E4G

#include "AbstractParseNode.h"
#include <vector>

class MaterialDataNode : public AbstractParseNode {

  public:
    MaterialDataNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    std::string getNumberOfFissiles () const;
    void setNumberOfFissiles (std::string);

  private:
    const std::string materialName;
    std::string numberOfFissiles;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // MATERIAL_DATA_NODE_H_9E4G
