#ifndef MACROSCOPIC_XS_NODE_H_I321
#define MACROSCOPIC_XS_NODE_H_I321

#include "AbstractParseNode.h"
#include <vector>

class MacroscopicCrossSectionNode : public AbstractParseNode {

  public:
    MacroscopicCrossSectionNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // MACROSCOPIC_XS_NODE_H_I321
