#include "ScatteringXSParser.h"

ScatteringXSParser::ScatteringXSParser(Lexer &lex){
  lexer = &lex;
}

bool ScatteringXSParser::parse(){

  try {
    if (lexer->getNextToken() != token::TRANSFER){
      printf("Expected Transfer Keyword.\n");
      return false;
    }
    root = new ScatteringXSNode(lexer->getCurrentTokenValue());

    AbstractParser* childParser = new IntegerParser(*lexer);
    if(!childParser->parse()){
      printf("Expected an integer after Transfer Keyword.\n");
      return false;
    }
    root->addChild(childParser->getRoot());

    while(lexer->peek() == token::NUMBER ||
          lexer->peek() == token::PROFILE ){

      if (lexer->peek() == token::NUMBER) {
        delete childParser;    
        childParser = new ValueParser(*lexer);
        if(!childParser->parse()){
          printf("Value Parser Failed in Scattering XS.\n");
          return false;
        }
      } else if (lexer->peek() == token::PROFILE) {
        delete childParser;
        childParser = new ProfileParser(*lexer);
        if(!childParser->parse()){
          printf("Profile Parser Failed in Scattering XS.\n");
          return false;
        }
      } else {
        printf("Unexpected Token in Scattering Cross Section Parser: %s\n",lexer->getNextTokenValue().c_str());
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* ScatteringXSParser::getRoot() const {
  return root;
}
