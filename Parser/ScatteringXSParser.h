#ifndef SCATTERING_XS_PARSER_H_3HN8
#define SCATTERING_XS_PARSER_H_3HN8

#include "AbstractParser.h"
#include "ScatteringXSNode.h"
#include "ValueParser.h"
#include "ProfileParser.h"
#include "../Lexer/Lexer.h"

class ScatteringXSParser : public AbstractParser {

  public:
    ScatteringXSParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // SCATTERING_XS_PARSER_H_3HN8
