#ifndef SCATTERING_XS_NODE_H_SQ3C
#define SCATTERING_XS_NODE_H_SQ3C

#include "AbstractParseNode.h"
#include "XSNode.h"
#include <vector>

class ScatteringXSNode : public AbstractParseNode {

  public:
    ScatteringXSNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue(std::ostream&) const;

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;

};



#endif // SCATTERING_XS_NODE_H_SQ3C
