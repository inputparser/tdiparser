#ifndef KINETIC_SOLVER_NODE_H_3AZ2
#define KINETIC_SOLVER_NODE_H_3AZ2

#include "AbstractParseNode.h"
#include <vector>

class KineticSolverNode : public AbstractParseNode {

  public:
    KineticSolverNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // KINETIC_SOLVER_NODE_H_3AZ2
