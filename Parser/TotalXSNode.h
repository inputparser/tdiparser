#ifndef TOTAL_XS_NODE_H_1B6G
#define TOTAL_XS_NODE_H_1B6G

#include "AbstractParseNode.h"
#include "XSNode.h"
#include <vector>

class TotalXSNode : public AbstractParseNode {

  public:
    TotalXSNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue(std::ostream&) const;

  private:
    const std::string value;
  //  unsigned int numberOfChildren;
  //  std::vector<AbstractParseNode*> children;
    AbstractParseNode* xsNode;
};



#endif // TOTAL_XS_NODE_H_1B6G
