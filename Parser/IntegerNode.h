#ifndef INTEGER_NODE_H_89TN
#define INTEGER_NODE_H_89TN

#include "AbstractParseNode.h"
#include <vector>

class IntegerNode : public AbstractParseNode {

  public:
    IntegerNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

  private:
    const std::string value;
    const unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // INTEGER_NODE_H_89TN
