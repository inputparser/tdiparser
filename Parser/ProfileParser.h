#ifndef PROFILE_PARSER_H_98HQ
#define PROFILE_PARSER_H_98HQ

#include "AbstractParser.h"
#include "ProfileNode.h"
#include "IntegerParser.h"
#include "../Lexer/Lexer.h"

class ProfileParser : public AbstractParser {

  public:
    ProfileParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // PROFILE_PARSER_H_98HQ
