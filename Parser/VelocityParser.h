#ifndef VELOCITY_PARSER_H_9J8K
#define VELOCITY_PARSER_H_9J8K

#include "AbstractParser.h"
#include "VelocityNode.h"
#include "ValueParser.h"
#include "../Lexer/Lexer.h"

class VelocityParser : public AbstractParser {

  public:
    VelocityParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // VELOCITY_PARSER_H_9J8K
