#ifndef FISSILE_ISOTOPE_NODE_H_WD39
#define FISSILE_ISOTOPE_NODE_H_WD39

#include "AbstractParseNode.h"
#include <vector>

class FissileIsotopeNode : public AbstractParseNode {

  public:
    FissileIsotopeNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // FISSILE_ISOTOPE_NODE_H_WD39
