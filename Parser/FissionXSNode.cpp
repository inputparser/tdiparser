#include "FissionXSNode.h"

FissionXSNode::FissionXSNode (std::string v) : value(v) {
  xsNode = new XSNode(value);
}

std::string FissionXSNode::getValue() const {
  return value;
}

unsigned int FissionXSNode::getNumberOfChildren() const {
  return xsNode->getNumberOfChildren();
}

AbstractParseNode* FissionXSNode::getChild(unsigned int index) const {
  if (index >= xsNode->getNumberOfChildren())
    throw ChildOverReachException("FissionXSNode", index, xsNode->getNumberOfChildren());

  return xsNode->getChild(index);
}

void FissionXSNode::addChild(AbstractParseNode* child){
  try {
    xsNode->addChild(child);
  } catch (Exception &e) {
    e.printErrorReport();
    throw e;
  }
}

void FissionXSNode::printValue (std::ostream &out) const {
  xsNode->printValue(out);
}
