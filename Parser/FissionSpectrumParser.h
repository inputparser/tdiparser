#ifndef FISSION_SPECTRUM_PARSER_H_98SA
#define FISSION_SPECTRUM_PARSER_H_98SA

#include "AbstractParser.h"
#include "FissionSpectrumNode.h"
#include "ValueParser.h"
#include "../Lexer/Lexer.h"

class FissionSpectrumParser : public AbstractParser {

  public:
    FissionSpectrumParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // FISSION_SPECTRUM_PARSER_H_98SA
