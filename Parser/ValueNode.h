#ifndef VALUE_NODE_H_L2J3
#define VALUE_NODE_H_L2J3

#include "AbstractParseNode.h"
#include <vector>

class ValueNode : public AbstractParseNode {

  public:
    ValueNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

  private:
    const std::string value;
    const unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;
};



#endif // VALUE_NODE_H_L2J3
