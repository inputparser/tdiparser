#ifndef VALUE_PARSER_H_234D
#define VALUE_PARSER_H_234D

#include "AbstractParser.h"
#include "ValueNode.h"
#include "../Lexer/Lexer.h"

class ValueParser : public AbstractParser {

  public:
    ValueParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // VALUE_PARSER_H_234D
