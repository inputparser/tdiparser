#include "FissileIsotopeNode.h"

FissileIsotopeNode::FissileIsotopeNode (std::string v) : value(v), numberOfChildren(0) {}

std::string FissileIsotopeNode::getValue() const {
  return value;
}

unsigned int FissileIsotopeNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* FissileIsotopeNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException ("FissileIsotopeNode",index,numberOfChildren);

  return children[index];
}

void FissileIsotopeNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}


