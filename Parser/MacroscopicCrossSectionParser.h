#ifndef MACROSCOPIC_XS_PARSER_H_A3ZE
#define MACROSCOPIC_XS_PARSER_H_A3ZE

#include "AbstractParser.h"
#include "MacroscopicCrossSectionNode.h"
#include "IntegerParser.h"
#include "MaterialDataParser.h"
#include "VelocityParser.h"
#include "../Lexer/Lexer.h"

class MacroscopicCrossSectionParser : public AbstractParser {

  public:
    MacroscopicCrossSectionParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // MACROSCOPIC_XS_PARSER_H_A3ZE
