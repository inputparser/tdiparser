#ifndef TOTAL_XS_PARSER_H_A3E3
#define TOTAL_XS_PARSER_H_A3E3

#include "AbstractParser.h"
#include "TotalXSNode.h"
#include "ValueParser.h"
#include "../Lexer/Lexer.h"

class TotalXSParser : public AbstractParser {

  public:
    TotalXSParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // TOTAL_XS_PARSER_H_A3E3
