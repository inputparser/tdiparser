#include "MaterialDataNode.h"

MaterialDataNode::MaterialDataNode (std::string v) : materialName(v), numberOfChildren(0) {
  numberOfFissiles = "0";
}

std::string MaterialDataNode::getValue() const {
  return materialName;
}

unsigned int MaterialDataNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* MaterialDataNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("MaterialDataNode", index, numberOfChildren);

  return children[index];
}

void MaterialDataNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}

std::string MaterialDataNode::getNumberOfFissiles () const {
  return numberOfFissiles;
}

void MaterialDataNode::setNumberOfFissiles (std::string v) {
  numberOfFissiles = v;
}

