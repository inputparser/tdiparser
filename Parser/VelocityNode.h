#ifndef VELOCITY_NODE_H_SD6T
#define VELOCITY_NODE_H_SD6T

#include "AbstractParseNode.h"
#include "XSNode.h"

class VelocityNode : public AbstractParseNode {

  public:
    VelocityNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue (std::ostream&) const;

  private:
    const std::string value;
  //  unsigned int numberOfChildren;
  //  std::vector<AbstractParseNode*> children;
    AbstractParseNode* xsNode;
};



#endif // VELOCITY_NODE_H_SD6T
