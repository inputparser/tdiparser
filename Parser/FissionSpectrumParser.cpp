#include "FissionSpectrumParser.h"

FissionSpectrumParser::FissionSpectrumParser(Lexer &lex){
  lexer = &lex;
}

bool FissionSpectrumParser::parse(){

  try {
    if (lexer->getNextToken() != token::FISSION_SPEC){
      printf("Expected FissionSpectrum Keyword.\n");
      return false;
    }
    root = new FissionSpectrumNode(lexer->getCurrentTokenValue());

    while(lexer->peek() == token::NUMBER){
      AbstractParser* childParser = new ValueParser(*lexer);
      if(!childParser->parse()){
        printf("Value Parser Failed in Fission XS.\n");
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport ();
    return false;
  }

  return true;
}

AbstractParseNode* FissionSpectrumParser::getRoot() const {
  return root;
}
