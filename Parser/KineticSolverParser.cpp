#include "KineticSolverParser.h"

KineticSolverParser::KineticSolverParser(Lexer &lex){
  lexer = &lex;
}

bool KineticSolverParser::parse(){

  try {
    if (lexer->getNextToken() != token::KINETIC_SOLVER){
      printf("Expected Kinetic Solver Keyword.\n");
      return false;
    }
    root = new KineticSolverNode(lexer->getCurrentTokenValue());

    AbstractParser* childParser = new ValueParser(*lexer);
    if(!childParser->parse()){
      printf("Expected a number after Kinetic Solver.\n");
      return false;
    }
    root->addChild(childParser->getRoot());

    if (lexer->getNextToken() != token::END){
      printf("Expected a closing bracket.\n");
      return false;
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* KineticSolverParser::getRoot() const {
  return root;
}
