#ifndef PROFILE_NODE_H_6J54
#define PROFILE_NODE_H_6J54

#include "AbstractParseNode.h"
#include <vector>

class ProfileNode : public AbstractParseNode {

  public:
    ProfileNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue(std::ostream&) const;

  private:
    const std::string value;
    unsigned int numberOfChildren;
    std::vector<AbstractParseNode*> children;

};



#endif // PROFILE_NODE_H_6J54
