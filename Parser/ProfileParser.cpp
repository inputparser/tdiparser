#include "ProfileParser.h"

ProfileParser::ProfileParser(Lexer &lex){
  lexer = &lex;
}

bool ProfileParser::parse(){

  try {
    if (lexer->getNextToken() != token::PROFILE){
      printf("Expected Profile Keyword.\n");
      return false;
    }
    root = new ProfileNode(lexer->getCurrentTokenValue());

    while(lexer->peek() == token::INTEGER){
      AbstractParser* childParser = new IntegerParser(*lexer);
      if(!childParser->parse()){
        printf("Integer Parser Failed in Profile.\n");
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* ProfileParser::getRoot() const {
  return root;
}
