#include "VelocityParser.h"

VelocityParser::VelocityParser(Lexer &lex){
  lexer = &lex;
}

bool VelocityParser::parse(){

  try {
    if (lexer->getNextToken() != token::VELOCITY){
      printf("Expected Velocity Keyword.\n");
      return false;
    }
    root = new VelocityNode(lexer->getCurrentTokenValue());

    while(lexer->peek() == token::NUMBER){
      AbstractParser* childParser = new ValueParser(*lexer);
      if(!childParser->parse()){
        printf("Value Parser Failed in Velocity.\n");
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* VelocityParser::getRoot() const {
  return root;
}
