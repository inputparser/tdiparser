#ifndef FISSION_XS_PARSER_H_P23O
#define FISSION_XS_PARSER_H_P23O

#include "AbstractParser.h"
#include "FissionXSNode.h"
#include "ValueParser.h"
#include "../Lexer/Lexer.h"

class FissionXSParser : public AbstractParser {

  public:
    FissionXSParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // FISSION_XS_PARSER_H_P23O
