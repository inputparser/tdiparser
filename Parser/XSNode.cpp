#include "XSNode.h"

XSNode::XSNode (std::string v) : value(v), numberOfChildren(0) {}

std::string XSNode::getValue() const {
  return value;
}

unsigned int XSNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* XSNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("XSNode", index, numberOfChildren);

  return children[index];
}

void XSNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}

void XSNode::printValue (std::ostream &out) const {
  out << value+"XS: ";
  for (unsigned i = 0; i < children.size(); i++)
    out << children[i]->getValue() << "  ";
  out << std::endl;
}
