#include "ProfileNode.h"

ProfileNode::ProfileNode (std::string v) : value(v), numberOfChildren(0) {}

std::string ProfileNode::getValue() const {
  return value;
}

unsigned int ProfileNode::getNumberOfChildren() const {
  return numberOfChildren;
}

AbstractParseNode* ProfileNode::getChild(unsigned int index) const {
  if (index >= numberOfChildren)
    throw ChildOverReachException("ProfileNode", index, numberOfChildren);

  return children[index];
}

void ProfileNode::addChild(AbstractParseNode* child){
  children.push_back(child);
  numberOfChildren++;
}

void ProfileNode::printValue (std::ostream& out) const {
  out << "ScatteringProfile: ";
  for (unsigned int i = 0; i < numberOfChildren; i++)
    out << children[i]->getValue() << "  ";
  out << std::endl;
}

