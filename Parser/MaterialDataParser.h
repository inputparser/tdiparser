#ifndef MATERIAL_DATA_PARSER_H_9K8Y
#define MATERIAL_DATA_PARSER_H_9K8Y

#include "AbstractParser.h"
#include "MaterialDataNode.h"
#include "FissileIsotopeParser.h"
#include "TotalXSParser.h"
#include "FissionXSParser.h"
#include "FissionSpectrumParser.h"
#include "ScatteringXSParser.h"
#include "../Lexer/Lexer.h"

class MaterialDataParser : public AbstractParser {

  public:
    MaterialDataParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // MATERIAL_DATA_PARSER_H_9K8Y
