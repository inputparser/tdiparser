#ifndef INTEGER_PARSER_H_98J4
#define INTEGER_PARSER_H_98J4

#include "AbstractParser.h"
#include "IntegerNode.h"
#include "../Lexer/Lexer.h"

class IntegerParser : public AbstractParser {

  public:
    IntegerParser (Lexer&);

    bool parse ();
    AbstractParseNode* getRoot () const;

  private:
    Lexer* lexer;
    AbstractParseNode* root;
};



#endif // INTEGER_PARSER_H_98J4
