#include "IntegerParser.h"

IntegerParser::IntegerParser(Lexer &lex){
  lexer = &lex;
}

bool IntegerParser::parse(){
  if (lexer->getNextToken() != token::INTEGER){
    printf("Expected an Integer.\n");
    return false;
  }

  root = new IntegerNode(lexer->getCurrentTokenValue());

  return true;
}

AbstractParseNode* IntegerParser::getRoot() const {
  return root;
}
