#include "MaterialDataParser.h"

MaterialDataParser::MaterialDataParser(Lexer &lex){
  lexer = &lex;
}

bool MaterialDataParser::parse(){
  try {
    if (lexer->getNextToken() != token::NAME){
      printf("Expected Material Name.\n"
             "\tGot: \"%s\"\n", lexer->getCurrentTokenValue().c_str());
      return false;
    }
    root = new MaterialDataNode(lexer->getCurrentTokenValue());

    while (lexer->peek() != token::NAME &&
           lexer->peek() != token::END ){

      AbstractParser* childParser = new FissileIsotopeParser(*lexer);
      if (lexer->peek() == token::FISSILE_ISOTOPE){
        delete childParser;
        childParser = new FissileIsotopeParser(*lexer);
        if(!childParser->parse()){
          printf("Fissile Isotope Parser Failed.\n");
          return false;
        }
      } else if (lexer->peek() == token::TOTAL_XS) {
        delete childParser;
        childParser = new TotalXSParser(*lexer);
        if(!childParser->parse()){
          printf("Total XS Parser Failed.\n");
          return false;
        }
      } else if (lexer->peek() == token::FISSION_XS) {
        delete childParser;
        childParser = new FissionXSParser(*lexer);
        if(!childParser->parse()){
          printf("Fission XS Parser Failed.\n");
          return false;
        }
      } else if (lexer->peek() == token::FISSION_SPEC) {
        delete childParser;
        childParser = new FissionSpectrumParser(*lexer);
        if(!childParser->parse()){
          printf("Fission Spectrum Parser Failed.\n");
          return false;
        }
      } else if (lexer->peek() == token::TRANSFER){
        delete childParser;
        childParser = new ScatteringXSParser(*lexer);
        if(!childParser->parse()){
          printf("Scattering Cross Section Parser Failed.\n");
          return false;
        }
      } else {
        printf("Unexpected Token in Material Data Parser: %s\n",lexer->getNextTokenValue().c_str()); 
        return false;
      }
      root->addChild(childParser->getRoot());
    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  return true;
}

AbstractParseNode* MaterialDataParser::getRoot() const {
  return root;
}
