#ifndef FISSION_SPECTRUM_NODE_H_9DAS
#define FISSION_SPECTRUM_NODE_H_9DAS

#include "AbstractParseNode.h"
#include "XSNode.h"

class FissionSpectrumNode : public AbstractParseNode {

  public:
    FissionSpectrumNode (std::string);

    std::string getValue () const;
    unsigned int getNumberOfChildren () const;
    AbstractParseNode* getChild (unsigned int) const;
    void addChild (AbstractParseNode*);

    void printValue (std::ostream&) const;

  private:
    const std::string value;
  //  unsigned int numberOfChildren;
  //  std::vector<AbstractParseNode*> children;
    AbstractParseNode* xsNode;
};



#endif // FISSFISSION_SPECTRUM_NODE_H_9DAS
