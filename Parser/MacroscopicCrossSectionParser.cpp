#include "MacroscopicCrossSectionParser.h"

MacroscopicCrossSectionParser::MacroscopicCrossSectionParser(Lexer &lex){
  lexer = &lex;
}

bool MacroscopicCrossSectionParser::parse(){

  try {
    if (lexer->getNextToken() != token::MACRO_XSEC){
      printf("Expected MacroscopicCrossSection Keyword.\n");
      return false;
    }
    root = new MacroscopicCrossSectionNode(lexer->getCurrentTokenValue());

    while (!lexer->finished()){
      AbstractParser* childParser = new IntegerParser(*lexer);

      if (lexer->peek() == token::INTEGER){
        delete childParser;
        childParser = new IntegerParser(*lexer);
        if(!childParser->parse()){
          printf("Expected an integer after MacroscopicCrossSection.\n"
                 "\tFound: \"%s\"\n", lexer->getCurrentTokenValue().c_str());
          return false;
        }
      } else if (lexer->peek() == token::VELOCITY){
        delete childParser;
        childParser = new VelocityParser(*lexer);
        if(!childParser->parse()){
          printf("Expected Velocity in MacroscopicCrossSection.\n"
                 "\tFound: \"%s\"\n", lexer->getCurrentTokenValue().c_str());
          return false;
        }
      } else if (lexer->peek() == token::NAME){
        delete childParser;
        childParser = new MaterialDataParser(*lexer);
        if(!childParser->parse()){
          printf("Expected Material Data in MacroscopicCrossSection.\n"
                 "\tFound: \"%s\"\n", lexer->getCurrentTokenValue().c_str());
          return false;
        }      
      } else if (lexer->peek() == token::END){
        lexer->advanceTokenStream();
        return true;
      } else {
        printf("Unexpected Token in Macroscopic Cross Section Parser: %s\n",lexer->getNextTokenValue().c_str());
        return false;
      }

      root->addChild(childParser->getRoot());

    }

  } catch (Exception &e) {
    e.printErrorReport();
    return false;
  }

  printf("Macroscopic Cross Section Parser Terminated Unexpectedly.\n");
  return false;
}

AbstractParseNode* MacroscopicCrossSectionParser::getRoot() const {
  return root;
}
