#include "Exception.h"

Exception::~Exception () {}

void Exception::printErrorReport () const {
  printf ("Error(%d): %s\n", errorCode, errorMessage.c_str());
}

std::string Exception::getErrorMessage () const {
  return errorMessage;
}

unsigned int Exception::getErrorCode () const {
  return errorCode;
}

void Exception::setErrorMessage (const std::string message) {
  errorMessage = message;
}

void Exception::setErrorCode (const unsigned int code) {
  errorCode = code;
}

