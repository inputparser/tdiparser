#ifndef CHILD_OVER_REACH_EXCEPTION_H_3GBS
#define CHILD_OVER_REACH_EXCEPTION_H_3GBS

#include "Exception.h"
#include <sstream>

class ChildOverReachException : public Exception {

  public:
    ChildOverReachException (const char*, const unsigned int, const unsigned int);
    ~ChildOverReachException ();
};

#endif // CHILD_OVER_REACH_EXCEPTION_H_3GBS

