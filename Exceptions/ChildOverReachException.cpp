#include "ChildOverReachException.h"

ChildOverReachException::ChildOverReachException (const char *location,
                                                  const unsigned int index,
                                                  const unsigned int limit){
  this->setErrorCode(1);
//  std::string message("Tried to access child "+index+" in TotalXSNode\n"+
//                        "when there are only "+limit+" total children.\n");
  std::stringstream stream;
  stream << "Tried to access child " << index << " in " << location << "\n";
  stream << "          ";
  stream << "when there are only " << limit << " total children.\n";
  this->setErrorMessage(stream.str());
}

ChildOverReachException::~ChildOverReachException () {}


