#ifndef EXCEPTION_H_B3G1
#define EXCEPTION_H_B3G1

#include <string>
#include <cstdio>

class Exception {

  public:
    virtual ~Exception();

    void printErrorReport() const;
    std::string getErrorMessage () const;
    unsigned int getErrorCode() const;

    void setErrorMessage (const std::string);
    void setErrorCode (const unsigned int);

  private:
    unsigned int errorCode;
    std::string errorMessage;

};

#endif // EXCEPTION_H_B3G1

