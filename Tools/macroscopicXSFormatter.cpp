#include "../Parser/MacroscopicCrossSectionParser.h"

bool dataFormatter (const AbstractParseNode*, std::fstream&);
bool materialDataFormatter (const AbstractParseNode*, std::fstream&);
bool velocityFormatter (const AbstractParseNode*, std::fstream&);
bool totalXSFormatter (const AbstractParseNode*, std::fstream&, std::string);
bool fissionSpectrumFormatter (const AbstractParseNode*, std::fstream&, std::string, unsigned int);
bool fissionXSFormatter (const AbstractParseNode*, std::fstream&, std::string, unsigned int);
bool scatteringXSFormatter (const AbstractParseNode*, std::fstream&, std::string);

int main (int argc, char* argv[]) {

  if (argc != 2){
    printf("Incorrect Number of Input Arguments. Usage:\n"
           "\t./macroscopicXSFormatter \"filename\"\n");
  }

  printf("Formatting File: \"%s\"\n", argv[1]);
  std::fstream inputFile(argv[1], std::ios_base::in);

  std::string outFileName("formattedXS.dat");
  std::fstream outputFile(outFileName.c_str(), std::ios_base::out);
  printf("Output File: %s\n",outFileName.c_str());


  Lexer lexer(inputFile);
  if(!lexer.lex()){
    printf("Lexer failed.\n");
    return 1;
  }

  AbstractParser* parser = new MacroscopicCrossSectionParser(lexer);
  if(!parser->parse()){
    printf("Parser failed.\n");
    return 1;
  }

  if(!dataFormatter(parser->getRoot(), outputFile)){
    printf("Formatter failed.\n");
    return 1;
  }

  inputFile.close();
  outputFile.close();

  return 0;
}

bool dataFormatter (const AbstractParseNode *root, std::fstream &out){

  unsigned int nGroups = atoi(root->getChild(0)->getValue().c_str());
  unsigned int nMedia = atoi(root->getChild(1)->getValue().c_str());

  for (unsigned int i = 2; i < nMedia+3; i++){
    if (root->getChild(i)->getValue() == "Velocity"){
      if(!velocityFormatter(root->getChild(i), out)){
        printf("Velocity Formatter Failed.\n");
        return false;
      }
    } else {
      if(!materialDataFormatter(root->getChild(i), out)){
        printf("Material Data Formatter Failed.\n");
        return false;
      }
    }

  }


  return true;
}

bool materialDataFormatter (const AbstractParseNode *root, std::fstream &out){

  std::string materialName = root->getValue();
  unsigned int nFissileIsotopes = 0;

  for (unsigned int i = 0; i < root->getNumberOfChildren(); i++){
    std::string value = root->getChild(i)->getValue();

    if (value == "Total"){
      if (!totalXSFormatter(root->getChild(i), out, materialName)){
        printf("Total XS Formatter Failed.\n");
        return false;
      }
    } else if (value == "FissileIsotopes") {
      nFissileIsotopes = atoi(root->getChild(i)->getChild(0)->getValue().c_str());
    } else if (value == "FissionSpectrum"){
      if (!fissionSpectrumFormatter(root->getChild(i), out, materialName, nFissileIsotopes)){
        printf("Fission Spectrum Formatter Failed.\n");
        return false;
      }
    } else if (value == "NuFission"){
      if (!fissionXSFormatter(root->getChild(i), out, materialName, nFissileIsotopes)){
        printf("Fission XS Formatter Failed.\n");
        return false;
      }
    } else if (value == "Transfer"){
      if (!scatteringXSFormatter(root->getChild(i), out, materialName)){
        printf("Scattering XS Formatter Failed.\n");
        return false;
      }
    } else {
      printf("Cross Section not Recognized: %s\n", value.c_str());
      //return false;
    }

  }

  return true;
}

bool velocityFormatter (const AbstractParseNode *root, std::fstream &out){

  out << "Vel = diag([";
  for (unsigned int i = 0; i < root->getNumberOfChildren(); i++)
    out << "  " << root->getChild(i)->getValue();
  out << "  ]);" << std::endl;

  return true;
}

bool totalXSFormatter (const AbstractParseNode *root, std::fstream &out, std::string baseName){

  out << baseName << "_Total = diag([";
  for (unsigned int i = 0; i < root->getNumberOfChildren(); i++)
    out << "  " << root->getChild(i)->getValue();
  out << "  ]);" << std::endl;

  return true;
}

bool fissionSpectrumFormatter (const AbstractParseNode *root, std::fstream &out, std::string baseName, unsigned int nFissile){

  unsigned int nGroups = root->getNumberOfChildren()/2;

  for (unsigned int j = 0; j < nFissile; j++){
    out << baseName << "_FissionSpectrum_" << j+1 << " = [";
    for (unsigned int i = 0; i < nGroups; i++)
      out << "  " << root->getChild(j*nGroups+i)->getValue() << ";";
    out << "  ];" << std::endl;
  }

  return true;
}

bool fissionXSFormatter (const AbstractParseNode *root, std::fstream &out, std::string baseName, unsigned int nFissile){

  unsigned int nGroups = root->getNumberOfChildren()/2;

  for (unsigned int j = 0; j < nFissile; j++){
    out << baseName << "_NuFission_" << j+1 << " = [";
    for (unsigned int i = 0; i < nGroups; i++)
      out << "  " << root->getChild(j*nGroups+i)->getValue() << ";";
    out << "  ];" << std::endl;
  }

  return true;
}

bool scatteringXSFormatter (const AbstractParseNode *root, std::fstream &out, std::string baseName){

  unsigned int nAnisotropy = atoi(root->getChild(0)->getValue().c_str());
  unsigned int nGroups = root->getChild(1)->getNumberOfChildren()/2;
  unsigned int counter = 0;

  for (unsigned int k = 0; k <= nAnisotropy; k++){
    out << baseName << "_Scatt_" << k << " = [";
    for (unsigned int i = 0; i < nGroups; i++){
      unsigned int begin = atoi(root->getChild(1)->getChild(2*i)->getValue().c_str());
      unsigned int end = atoi(root->getChild(1)->getChild(2*i+1)->getValue().c_str());

      if (i != 0) out << std::string(baseName.length()+12, ' ');

      for (unsigned int j = 1; j < begin; j++)
        out << "0.0," << std::string(root->getChild(2)->getValue().length()-1, ' ');
      for (unsigned int j = begin; j <= end; j++){
        out << root->getChild(2+counter)->getValue() << ",  ";
        counter++;
      }
      for (unsigned int j = end; j < nGroups; j++)
        out << "0.0," << std::string(root->getChild(2)->getValue().length()-1, ' ');

      out << ";" << std::endl;
    }
    out << std::string(baseName.length()+12, ' ') << "];" << std::endl;
  }

  return true;
}


