#ifndef LEXER_H_42E3
#define LEXER_H_42E3

#include <stdio.h>
#include <vector>
#include <string>
#include "FlexLexer.h"
#include <fstream>
#include "tokens.h"

extern int lineNumber;

class Lexer {

  public:
    Lexer(const char*);
    Lexer(const std::istream&);
    ~Lexer ();

    bool lex ();

    int getCurrentToken () const;
    std::string getCurrentTokenValue () const;
    int getNextToken ();
    std::string getNextTokenValue ();
    int peek () const;
    unsigned int getNumberTokens () const;
    unsigned int getCurrentLineNumber () const;

    bool finished () const;
    void advanceTokenStream ();
    void resetTokenStream ();
    void printTokens() const;
 
  private:
    std::vector<std::pair<int, std::string> > tokenStream;
    std::vector<unsigned int> lineNumbers;
    unsigned int currentTokenIndex;
    unsigned int numberTokens;
    bool done;

    yyFlexLexer baseLexer;
    std::fstream inputFile;
    std::iostream* inputStream;

};

#endif // LEXER_H_42E3

