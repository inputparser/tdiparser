%{
#include "tokens.h"
int lineNumber = 1;
%}

%option c++
%option noyywrap
%option stack

DIGIT			[0-9]
SIGN			[+\-]?
INTEGER			{SIGN}{DIGIT}+
FLOAT			{SIGN}{DIGIT}+(\.{DIGIT}+)?
SCIENTIFIC		{FLOAT}[eE]{INTEGER}
NUMBER			{SCIENTIFIC}|{INTEGER}|{FLOAT}
KINETIC_SOLVER		"KineticSolver"|"kineticsolver"|"kineticSolver"
MACRO_XSEC      	"MacroscopicCrossSections"|"macroscopiccrosssections"|"MACROSCOPICCROSSSECTIONS"
FISSILE_ISOTOPE 	"FissileIsotopes"|"fissileisotopes"|"FISSILEISOTOPES"
TOTAL_XS		"Total"|"total"|"TOTAL"
FISSION_XS		"NuFission"|"nufission"|"NUFISSION"
FISSION_SPEC		"FissionSpectrum"|"fissionspectrum"|"FISSIONSPECTRUM"
VELOCITY		"Velocity"|"velocity"|"VELOCITY"
TRANSFER		"Transfer"|"transfer"|"TRANSFER"
PROFILE			"Profile"|"profile"|"PROFILE"
OPEN_STATE              "["
CLOSE_STATE             "]"
END				"Stop"|"stop"|"STOP"
NAME                    [A-Za-z_][A-Za-z_0-9]+

%s Comment
%s KineticSolver
%s MacroXSec



%%

"#"						yy_push_state(Comment);
<Comment>[^\n]*			;
<Comment>[\n]			{lineNumber++; yy_pop_state();}

{KINETIC_SOLVER}	{yy_push_state(KineticSolver); return token::KINETIC_SOLVER;}
<KineticSolver>{OPEN_STATE}   ; 
<KineticSolver>{NUMBER} return token::NUMBER;
<KineticSolver>[ \t]+	;
<KineticSolver>[\n]		{lineNumber++;}
<KineticSolver>{CLOSE_STATE}    {yy_pop_state(); return token::END;}

{MACRO_XSEC}            {yy_push_state(MacroXSec); return token::MACRO_XSEC;}
<MacroXSec>{OPEN_STATE}   ;
<MacroXSec>{INTEGER}   return token::INTEGER;
<MacroXSec>{NUMBER}    return token::NUMBER;
<MacroXSec>[ \t]+   ;
<MacroXSec>[\n]             {lineNumber++;}
<MacroXSec>{FISSILE_ISOTOPE} return token::FISSILE_ISOTOPE;
<MacroXSec>{TOTAL_XS}		return token::TOTAL_XS;
<MacroXSec>{FISSION_XS}		return token::FISSION_XS;
<MacroXSec>{FISSION_SPEC}	return token::FISSION_SPEC;
<MacroXSec>{VELOCITY}		return token::VELOCITY;
<MacroXSec>{TRANSFER}		return token::TRANSFER;
<MacroXSec>{PROFILE}		return token::PROFILE;
<MacroXSec>{NAME}           {return token::NAME;}
<MacroXSec>{CLOSE_STATE}    {yy_pop_state(); return token::END;}



{END}					return token::END;
[\n]					lineNumber++;
[ \t]+					;

.                 		{printf("Syntax Error(%d): Unidentified Character \"%s\".\n",lineNumber,yytext);
                    	 return token::ERROR;}


%%



