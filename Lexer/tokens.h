//#define TYPE 1
//#define NAME 2
//#define TABLE_PREFIX 3
//#define PORT 4
//#define COLON 5
//#define IDENTIFIER 6
//#define INTEGER 7
#ifndef TOKEN_H_12A5
#define TOKEN_H_12A5

class token{

  public:
    enum TokenType {EMPTY,
                    ERROR,
                    START,
                    END,
                    COLON,
                    KINETIC_SOLVER,
                    MACRO_XSEC,
                    FISSILE_ISOTOPE,
                    TOTAL_XS,
                    FISSION_XS,
                    FISSION_SPEC,
                    VELOCITY,
                    TRANSFER,
                    PROFILE,
                    IDENTIFIER,
                    INTEGER,
                    NUMBER,
                    NAME};
    static const unsigned int nTokens = 18;
    static const char* TokenName[nTokens];
};

#endif // TOKEN_H_12A5
