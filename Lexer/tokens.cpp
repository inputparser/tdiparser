#include "tokens.h"

const char* token::TokenName[nTokens] = {"",
                                         "Error",
                                         "Start",
                                         "End",
                                         ":",
                                         "KineticSolver",
                                         "MacroscopicCrossSections",
                                         "FissileIsotopes",
                                         "Total",
                                         "NuFission",
                                         "FissionSpectrum",
                                         "Velocity",
                                         "Transfer",
                                         "Profile",
                                         "Identifier",
                                         "Integer",
                                         "Number",
                                         "Name"};


