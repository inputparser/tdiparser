#include "../Lexer.h"
#include <sstream>

void writeInput(std::iostream &input){
  input << "#This is a comment." << std::endl;
  input << "# Second comment;" << std::endl;
  input << "#" << std::endl;
  input << "" << std::endl;
  input << "KineticSolver[ 2.5 ]" << std::endl;
  input << "# late comment" << std::endl;
  input << "" << std::endl;
  input << "Stop" << std::endl;
  input << "" << std::endl;
}

int main (){

  std::stringstream input;
  writeInput(input);

  Lexer lexer(input);
  if(!lexer.lex()){
    printf("Lexer failed.\n");
    return 1;
  }

  // Check that we get the right number of tokens
  if (lexer.getNumberTokens () != 5){
    printf("Incorrect number of tokens found.\n"
    	   "\tExpected: %d\n"
    	   "\tFound:    %d\n",5, lexer.getNumberTokens() );
    return 1;
  }

  // Check that print works
  lexer.printTokens();

  // Check that traversing through lexer gives correct tokens.
  int tokens[] = {token::START, token::KINETIC_SOLVER, token::NUMBER, token::END, token::END};
  // Check the first token is correct
  if (lexer.getCurrentToken() != tokens[0]){
	  printf("Token incorrect.\n"
			 "\tExpected: %d (%s)\n"
			 "\tFound:    %d (%s)\n", tokens[0], token::TokenName[tokens[0]],
			                          lexer.getCurrentToken(), token::TokenName[lexer.getCurrentToken()]);
	  return 1;
  }
  // Traverse the rest of the token stream
  for (unsigned int i = 1; i < lexer.getNumberTokens(); i++)
	  if (lexer.getNextToken() != tokens[i]){
		  printf("Token incorrect.\n"
				 "\tExpected: %d (%s)\n"
				 "\tFound:    %d (%s)\n", tokens[i], token::TokenName[tokens[i]],
				                          lexer.getCurrentToken(), token::TokenName[lexer.getCurrentToken()]);
		  return 1;
	  }
  if (lexer.finished()){
	  printf("Lexer Not expected to be finished.\n");
	  return 1;
  }
  lexer.getNextToken();
  if (!lexer.finished()){
	  printf("Lexer Expected to be finshed.\n");
	  return 1;
  }

  // Check that line numbers are correct
  lexer.resetTokenStream ();
  lexer.advanceTokenStream();
  if (lexer.getCurrentLineNumber() != 5){
    printf("Line number incorrect\n\tExpected: 5, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  }
  lexer.advanceTokenStream();
  if (lexer.getCurrentLineNumber() != 5){
    printf("Line number incorrect\n\tExpected: 5, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  }

  lexer.advanceTokenStream();
  lexer.advanceTokenStream();
  if (lexer.getCurrentLineNumber() != 8){
    printf("Line number incorrect\n\tExpected: 8, Result: %d\n",lexer.getCurrentLineNumber());
    return 1;
  } 

  return 0;
}
