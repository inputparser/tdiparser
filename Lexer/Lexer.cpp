#include "Lexer.h"

Lexer::Lexer (const char* input){
  printf("Input File: %s\n",input);

  inputFile.open(input);

  inputStream = new std::iostream(inputFile.rdbuf());
  baseLexer = yyFlexLexer(inputStream);

  currentTokenIndex = 0;
  tokenStream.push_back(std::pair<int, std::string>(token::START, token::TokenName[token::START]));
  lineNumbers.push_back(0);
  numberTokens = 1;
  done = false;
}

Lexer::Lexer (const std::istream& input){
  printf("Input File: %s\n","Using inputStream");

  input.rdbuf()->pubseekpos(0, std::ios_base::in);
  inputStream = new std::iostream(input.rdbuf());

  baseLexer = yyFlexLexer(inputStream);

  currentTokenIndex = 0;
  tokenStream.push_back(std::pair<int, std::string>(token::START, token::TokenName[token::START]));
  lineNumbers.push_back(0);
  numberTokens = 1;
  done = false;
}

Lexer::~Lexer (){
	if(inputFile.is_open())
		inputFile.close();
	delete inputStream;
}

bool Lexer::lex (){

  int ntoken = baseLexer.yylex();
  std::pair<int, std::string> tokenPair;

  while(ntoken){
    if (ntoken == token::ERROR)
      return false;

    tokenPair.first = ntoken;
    if (*(baseLexer.YYText()) == '\n')
    	tokenPair.second = "endLine";
    else
    	tokenPair.second = baseLexer.YYText();
    tokenStream.push_back(tokenPair);
    lineNumbers.push_back(lineNumber);
    ntoken = baseLexer.yylex ();
    numberTokens++;
  }
  printf("Number of Tokens: %u\n",numberTokens);

  return true;
}

int Lexer::getCurrentToken () const {
  return tokenStream[currentTokenIndex].first;
}

int Lexer::peek () const {
  return tokenStream[currentTokenIndex+1].first;
}

std::string Lexer::getCurrentTokenValue () const {
	return tokenStream[currentTokenIndex].second;
}

int Lexer::getNextToken () {
  advanceTokenStream();
  return tokenStream[currentTokenIndex].first;
}



std::string Lexer::getNextTokenValue() {
  advanceTokenStream();
  return tokenStream[currentTokenIndex].second;
}

unsigned int Lexer::getNumberTokens () const {
  return numberTokens;
}

unsigned int Lexer::getCurrentLineNumber () const {
  return lineNumbers[currentTokenIndex];
}

void Lexer::advanceTokenStream (){
  if (currentTokenIndex < numberTokens)
    currentTokenIndex++;
  if (currentTokenIndex == numberTokens)
	  done = true;
}

void Lexer::printTokens () const{
  for (unsigned int i = 0; i < numberTokens; i++)
	  printf("Line: %d\tToken Id: %d\tToken Type: %-17s\tToken Value: %s\n",
			  lineNumbers[i],
			  tokenStream[i].first,
			  token::TokenName[tokenStream[i].first],
			  tokenStream[i].second.c_str());
}

void Lexer::resetTokenStream () {
  currentTokenIndex = 0;
  done = false;
}

bool Lexer::finished() const {
	return done;
}

